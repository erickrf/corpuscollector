# -*- coding: utf-8 -*- 

'''
Coleta texto a partir de textos publicados no G1.
Acessa as notícias através do RSS.
'''

import argparse
import os
import sys
import json
import logging
import datetime as dt
import xml.etree.ElementTree as ET
from email.utils import parsedate_tz
import bs4

import utils

urls = {
        'g1-noticias': 'http://g1.globo.com/dynamo/rss2.xml',
        'g1-esportes': 'http://globoesporte.globo.com/servico/semantica/editorias/plantao/feed.rss',
        'uol': 'http://rss.uol.com.br/feed/noticias.xml'
        }

def salva_ultimo_acesso(xml, site):
    u'''
    Salva a hora da última notícia acessada do site dado.
    Recebe o elemento raiz do XML do RSS.
    '''
    timestamp_elem = xml.find('channel/item/pubDate')
    if timestamp_elem is None:
        # não há informação de timestamp. apenas ignore.
        return
    
    timestamp = timestamp_elem.text
    hora_tupla = parsedate_tz(timestamp)
    hora = dt.datetime(*hora_tupla[:6])
    
    if os.path.isfile('hora-acesso.json'):
        with open('hora-acesso.json', 'rb') as f:
            data = json.load(f)
    else:
        data = {}
        
    data[site] = str(hora)
    with open('hora-acesso.json', 'wb') as f:
        json.dump(data, f)

def carrega_ultimo_acesso(site):
    u'''
    Carrega a hora do último acesso ao site dado
    a partir de um arquivo de configuração.
    '''
    try:
        with open('hora-acesso.json', 'rb') as f:
            data = json.load(f)
    except IOError:
        # arquivo não existe. retorna um ano extremamente antigo
        return dt.datetime(1, 1, 1)
    
    try:
        hora_str = data[site]
    except KeyError:
        return dt.datetime(1, 1, 1)
    
    return dt.datetime.strptime(hora_str, '%Y-%m-%d %H:%M:%S')

def extrai_conteudo(html, site):
    '''
    Extrai o conteúdo útil da página HTML dada.
    Cada site tem um tratamento diferente.
    '''
    soup = bs4.BeautifulSoup(html, 'html5lib')
    
    if site.startswith('g1'):
        conteudo = soup.find(id='materia-letra')
        extrator = utils.extrai_texto_paragrafos
    elif site == 'uol':
        conteudo = soup.find(id='texto')
        
        if conteudo is None:
            # algumas páginas da Folha, dentro da UOL, têm estrutura diferente
            conteudo = soup.find(itemprop="articleBody")
            extrator = utils.extrai_texto_paragrafos
        else:
            # UOL normal
            extrator = utils.extrai_texto
    else:
        raise ValueError(u'Site não reconhecido: %s' % site)
    
    if conteudo is None:
        # página com formato diferente. ignore.
        raise ValueError(u'Formato do HTML diferente do padrão. Pulando')
    
    texto = extrator(conteudo)
    return texto

def salva_noticias(rss_xml, destino, site):
    '''
    Acessa os itens descritos no RSS e salva o texto das notícias 
    no diretório destino.
    '''
    root = ET.fromstring(rss_xml)
    ultimo_acesso = carrega_ultimo_acesso(site)
    
    for item in root.iterfind('channel/item'):
        titulo = item.find('title').text
        
        # remove quebras de linha no título
        titulo = titulo.replace('\n', ' ')
        
        link = item.find('link').text
        timestamp_elem = item.find('pubDate')
        
        if timestamp_elem is not None:
            # se há um timestamp, vamos verificar se a notícia já foi pega antes
            timestamp = timestamp_elem.text
            
            # substitui nomes de meses em português que não são parseados
            timestamp = timestamp.replace('Fev', 'Feb').replace('Abr', 'Apr').replace('Mai', 'May')
            timestamp = timestamp.replace('Ago', 'Aug').replace('Set', 'Sep').replace('Out', 'Oct')
            timestamp = timestamp.replace('Dez', 'Dec')

            # hora da publicacao
            hora_noticia = parsedate_tz(timestamp)
            hora_noticia = dt.datetime(*hora_noticia[:6])
            if hora_noticia < ultimo_acesso:
                # notícia mais antiga que a última leva de acessos
                logging.debug(u'Pulando notícia antiga...')
                continue
        
        logging.info('Acessando: %s' % titulo)
        html = utils.acessa_conteudo(link)
        
        try:
            texto = extrai_conteudo(html, site)
        except ValueError, e:
            logging.error(e.message.encode('utf-8'))
            continue
        
        # nome de arquivo precisa estar no encoding do SO
#        encoding = sys.getfilesystemencoding()
        encoding = 'utf-8'
        if encoding is None:
            encoding = 'utf-8'
        
        titulo_normalizado = utils.normaliza_nome_arquivo(titulo)
        titulo_encoded = titulo_normalizado.encode(encoding)
        arquivo = os.path.join(destino, '%s.txt' % titulo_encoded)
        
        with open(arquivo, 'wb') as f:
            f.write(titulo.encode('utf-8'))
            f.write('\n')
            f.write(texto.encode('utf-8'))
    
    salva_ultimo_acesso(root, site)
    
if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-d', '--destino', required=True, 
                        help=u'Diretório para salvar os textos baixados.')
    parser.add_argument('-s', '--site', required=True,
                        help=u'Site de onde serão obtidos textos.',
                        choices=urls.keys())
    args = parser.parse_args()
    
    logging.basicConfig(level=logging.DEBUG, 
                        format="%(message)s")
    
    rss_url = urls[args.site]
    rss_xml = utils.acessa_conteudo(rss_url)
    salva_noticias(rss_xml, args.destino, args.site)
    
