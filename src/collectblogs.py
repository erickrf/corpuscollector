# -*- coding: utf-8 -*-

'''
Script para baixar textos de blogs do g1.
'''

import argparse
import os
import urllib2
import logging
from bs4 import BeautifulSoup

import utils

def acessa_historico(url, nome_base, diretorio):
    '''
    Acessa e salva conteúdo histórico do blog.
    '''
    if not url.endswith('/'):
        url += '/'
    
    ano = 2014
    mes = 1
    while True:
        # acesse cada mês até ter um erro 404
        url_mes = '%s%d/%02d/' % (url, ano, mes)
        
        logging.info(u'Acessando postagens de %02d/%d' % (mes, ano))
        try:
            html = utils.acessa_conteudo(url_mes)
        except urllib2.HTTPError:
            break
        
        soup = BeautifulSoup(html)
        texto = utils.extrai_texto_paragrafos(soup)
        
        nome_arquivo = u'%s-%d-%02d.txt' % (nome_base, 
                                          ano,
                                          mes)
        caminho = os.path.join(diretorio, nome_arquivo)
        with open(caminho, 'wb') as f:
            f.write(texto.encode('utf-8'))
        
        mes -= 1
        if mes == 0:
            ano -= 1
            mes = 12
        

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('url', help=u'URL do blog')
    parser.add_argument('data', help=u'Diretório para salvar textos')
    parser.add_argument('nome', help=u'Nome base dos arquivos salvos')
    args = parser.parse_args()
    
    logging.basicConfig(format='%(message)s', level=logging.DEBUG)
    logging.info(u'Acessando blog em %s' % args.url)
    acessa_historico(args.url, args.nome, args.data)
    
    