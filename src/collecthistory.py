# -*- coding: utf-8 -*-

u'''
Script para coletar publicações no histórico de revistas do G1.
Usa acesso a funções internas do G1 para obter um JSON com o histórico.
'''

import json
import os
import argparse
import logging
from bs4 import BeautifulSoup

import utils

url_basica = 'http://%s.globo.com/dinamico/plantao/%s/%s/qtd/%d/%d/'

def extrai_texto(html):
    u'''
    Recebe o html de um item do json e retorna o texto útil encontrado.
    '''
    try:
        soup = BeautifulSoup(html, 'html5lib')
    except TypeError:
        logging.info('Erro ao interpretar XML.')
        return None
    
    paragrafos = soup.find_all('p')
    textos = []
    for paragrafo in paragrafos:
        # Substitui tags <br> por quebras de linha e remove
        # quebras de linha direto do texto
        
        # use um placeholder para não confundir os <br> com quebras de 
        # linha normais. 
        placeholder = '**BR_PLACEHOLDER**'
        while True:
            try:
                assert placeholder not in paragrafo.text
                break
            except:
                placeholder += '_'
        
        [br.replace_with(placeholder) for br in paragrafo.find_all('br')]
        texto = paragrafo.text.strip().replace('\n', ' ')
        texto = texto.replace(placeholder, '\n')
        textos.append(texto)
    
    texto_final = '\n'.join(textos)
    return texto_final

def processa_noticias(data, destino):
    u'''
    Processa todas as notícias encontradas dentro de um objeto
    json.
    destino indica o diretório onde arquivos devem ser
    salvos.
    '''
    for item in data['conteudos']:
        titulo = item['titulo'].strip()
        logging.debug('Processando %s...' % titulo)
        
        texto = extrai_texto(item['corpo'])
        if texto is None:
            # artigos sem texto. normalmente, são apenas galerias de fotos.
            continue
        
        texto = '%s\n%s' % (titulo, texto)
        
        # remove possiveis tags html do título
        titulo_soup = BeautifulSoup(titulo, 'html5lib')
        titulo = utils.normaliza_nome_arquivo(titulo_soup.text)
        arquivo = os.path.join(destino, '%s.txt' % titulo)
        with open(arquivo, 'wb') as f:
            f.write(texto.encode('utf-8'))


def acessa_todas_noticias(dominio, nome, destino):
    u'''
    Acessa todas as notícias de uma revista.
    Domínio é o subdomínio da revista (sob globo.com)
    Nome é o nome que aparece nas subpastas da url
    Destino é onde os arquivos devem ser salvos
    '''
    url = url_basica % (args.dominio, args.nome, args.nome, 1, 1)
    json_file = utils.acessa_conteudo(url)
    data = json.loads(json_file)
    
    # total_items indica a quantidade de artigos na base de dados.
    # vamos pegar em lotes.
    total_items = int(data['totalItens'])
    
    items_lidos = 0
    lote = 1
    items_por_lote = 500
    while items_lidos < total_items:
        url = url_basica % (args.dominio, args.nome, args.nome, 
                            items_por_lote, lote)
        json_file = utils.acessa_conteudo(url)
        data = json.loads(json_file)
        
        processa_noticias(data, destino)
        
        lote += 1
        items_lidos += items_por_lote
    

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('-d', '--destino', type=str, required=True,
                        help=u'Diretótio para salvar os textos')
    parser.add_argument('-n', '--nome', type=str, required=True,
                        help=u'Nome da revista para acessar')
    parser.add_argument('-u', '--dominio', required=True,
                        help=u'Subdomínio da revista (abaixo de globo.com)')
    args = parser.parse_args()
    
    logging.basicConfig(level=logging.DEBUG, 
                        format="%(message)s")
    
    acessa_todas_noticias(args.dominio, args.nome, args.destino)
    
    
    