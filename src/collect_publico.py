# -*- coding: utf-8 -*-

from __future__ import unicode_literals

'''
Collect news articles from the Portuguese Público newspaper.
'''

import json
import logging
import argparse
import os
import bs4
import newspaper
import time
import urllib2

import utils

accessed_urls = set()

def retrieve_html_for_page(page):
    '''
    Retrieve the HTML data for the page with the given number.
    Each page is a list of 10 articles.
    '''
    url_base = 'http://www.publico.pt/json/noticias/lista/315?pagina={page}&Fonte=TAGS'
    json_string = utils.acessa_conteudo(url_base.format(page=page))
    json_data = json.loads(json_string)
    return json_data['html']

def extract_article(elem, output_dir):
    '''
    Extract and save the article in an "a" element. 
    '''
    title = elem.h2.text
    link = elem.get('href')
    
    if link in accessed_urls:
        logger.info('Already accessed, skipping: %s' % link)
        return
    else:
        accessed_urls.add(link)
    
    article = newspaper.Article(url=link, language='pt', fetch_images=False,
                                browser_user_agent=utils.user_agent)
    article.download()
    article.parse()
    path = os.path.join(output_dir, utils.normaliza_nome_arquivo(title) + '.txt')
    if os.path.isfile(path):
        logger.warn('File already exists, skipping: %s' % path)
        return
        
    with open(path, 'wb') as f:
        f.write(article.text.encode('utf-8'))
    
    logger.info('Saved article %s' % title)
    

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('output', help='Output directory')
    parser.add_argument('-f', help='From page (each page in the article history lists 10 items)',
                        default=1, type=int, dest='from_page')
    parser.add_argument('-q', help='Quiet mode', action='store_true', dest='quiet')
    args = parser.parse_args()
    
    utils.set_logger('publico')
    logger = logging.getLogger('publico')
    if not args.quiet:
        logger.setLevel(logging.INFO)
    
    for page in range(args.from_page, 46904):
        logger.info('Accessing page %d' % page)
        try:
            html = retrieve_html_for_page(page)
        except Exception as e:
            logger.error(e)
            time.sleep(10)
            continue
        
        soup = bs4.BeautifulSoup(html, 'lxml')
        # take the "a" elements directly under "article"
        a_elems = soup.select('article > a')
        for elem in a_elems:
            extract_article(elem, args.output)
            time.sleep(0.8)
    
