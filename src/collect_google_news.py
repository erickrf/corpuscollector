# -*- coding: utf-8 -*-

from __future__ import unicode_literals

'''
Script to download news from Google News.
'''

import re
import os
from six.moves import cPickle
import argparse
import logging
from xml.etree import cElementTree as ET
from collections import defaultdict
from urllib.parse import urlparse
import bs4
import newspaper

from . import utils

_accessed_urls_filename = 'accessed-urls.dat'

rss_urls = {
    "pt-br": ["https://news.google.com/news?cf=all&hl=pt-BR&ned=pt-BR_br&output=rss",
              "https://news.google.com/news?cf=all&hl=pt-BR&ned=pt-BR_br&topic=t&output=rss",
              "https://news.google.com/news?cf=all&hl=pt-BR&ned=pt-BR_br&topic=e&output=rss",
              "https://news.google.com/news?cf=all&hl=pt-BR&ned=pt-BR_br&topic=m&output=rss",
              "https://news.google.com/news?cf=all&hl=pt-BR&ned=pt-BR_br&topic=s&output=rss"],

    "pt-pt": ["https://news.google.com/news?cf=all&hl=pt-PT&ned=pt-PT_pt&output=rss",
              "https://news.google.com/news?cf=all&hl=pt-PT&ned=pt-PT_pt&topic=m&output=rss",
              "https://news.google.com/news?cf=all&hl=pt-PT&ned=pt-PT_pt&topic=t&output=rss",
              "https://news.google.com/news?cf=all&hl=pt-PT&ned=pt-PT_pt&topic=e&output=rss",
              "https://news.google.com/news?cf=all&hl=pt-PT&ned=pt-PT_pt&topic=s&output=rss"]
}

filtered_domains_regexp = {
                           "pt-br": r"^pt\.|\.pt$",
                           "pt-pt": r"(?i)^br\.|(\.br|globo.com)$|pt_br",
                           }

filtered_domains = {"pt-br": None,
                    "pt-pt": set(['globo.com', 'bbc.com', 'meiobit.com',
                                  'arena4g.com', 'supernovo.net'])}


def extract_articles(content, regexp_filter, filtered_domains):
    '''
    Read the content of a news cluster (extracted from Google News RSS), follow
    the links inside it and return a list of newspaper Articles.
    
    :param content: the content of an item element from the RSS
    :param regexp_filter: a regexp matching domains that should be ignored
    :param filtered_domains: domain to be filtered out
    '''
    # content has some HTML code
    soup = bs4.BeautifulSoup(content, 'lxml')
    
    logger = logging.getLogger('crawler')
    
    # the last one is an internal Google News link
    a_tags = soup.find_all('a')[:-1]
    
    articles = []
    for a in a_tags:
        link = a['href']
        
        url_parts = urlparse(link)
        domain = url_parts.netloc
        
        if filtered_domains is not None and domain in filtered_domains:
            logger.info('URL %s skipped for this language' % link)
            continue
        
        if regexp_filter is not None and re.search(regexp_filter, domain):
            logger.info('URL %s skipped for this language' % link)
            continue
        
        article = newspaper.Article(url=link, language='pt', fetch_images=False,
                                    browser_user_agent=utils.user_agent)
        article.apparent_title = a.text
        articles.append(article)
    
    return articles


def find_news_cluster(item):
    '''
    Parse the content of an RSS item and extract its news cluster.

    Some news do not belong to a cluster. For these, this function 
    returns None.
    '''
    # the news cluster is identified by the guid element
    guid = item.find('guid')

    return guid.text


def save_article(directory, cluster, text, title):
    '''
    Save a given article text in the appropriate location.
    
    :param directory: root directory; clusters directories are
        created within it
    :param cluster: cluster the text belongs to. The text will be
        saved in a subdirectory corresponding to the cluster.
    :param text: the text itself, as a unicode object.
    :param title: the title used to create the filename
    '''
    logger = logging.getLogger('crawler')
    
    if text == '':
        logger.info('Skipping empty text')
        return
    
    cluster = utils.normaliza_nome_arquivo(cluster)
    cluster_dir = os.path.join(directory, cluster)
    if not os.path.isdir(cluster_dir):
        os.mkdir(cluster_dir)
    
    normalized_title = utils.normaliza_nome_arquivo(title)
    filename = os.path.join(cluster_dir, normalized_title)
    filename += '.txt'
    
    # check if there is already a file with that name
    while os.path.isfile(filename):
        logger.info('Renaming {}'.format(filename))
        filename = filename.replace('.txt', '(new).txt')
        
    with open(filename, 'wb') as f:
        f.write(text.encode('utf-8'))

def load_accessed_urls():
    '''
    Load a dictionary containing all previously accessed URL's.
    '''
    global _accessed_urls
    global _accessed_urls_filename
    
    if os.path.isfile(_accessed_urls_filename):
        with open(_accessed_urls_filename, 'rb') as f:
            try:
                _accessed_urls = cPickle.load(f)
            except EOFError:
                # empty file
                _accessed_urls = defaultdict(set)
    else:
        # dictionary maps cluster codes to their set of URLs
        # defaultdict provides an empty set for each new cluster
        _accessed_urls = defaultdict(set)


def save_accessed_urls():
    '''
    Save the dictionary listing accessed URL's.
    '''
    global _accessed_urls
    global _accessed_urls_filename
    
    with open(_accessed_urls_filename, 'wb') as f:
        cPickle.dump(_accessed_urls, f, -1)


def is_repeated(cluster, url):
    '''
    Return whether the given URL has already been accessed.
    If it has not, add to the accessed URL's dictionary.
    '''
    global _accessed_urls
    cluster_set = _accessed_urls[cluster]
    already_accessed = url in cluster_set
    if not already_accessed:
        cluster_set.add(url)
    
    return already_accessed


if __name__ == '__main__':
    
    parser = argparse.ArgumentParser()
    parser.add_argument('output_dir', help='Directory to save downloaded articles')
    args = parser.parse_args()
    
    utils.set_logger('crawler')
    logger = logging.getLogger('crawler')
    load_accessed_urls()
    
    for language in rss_urls:
        rss_list = rss_urls[language]
        
        filtered_domains_lang = filtered_domains[language]
        filtered_domains_re_lang = filtered_domains_regexp[language]
        output_dir = os.path.join(args.output_dir, language)
        
        for rss_url in rss_list: 
            
            logger.info('Accessing %s Google News RSS...' % language)
            rss_xml = utils.acessa_conteudo(rss_url)
        
            tree = ET.fromstring(rss_xml)
            items = tree.findall('channel/item')
            logger.info('Accessing news')
        
            count = 0
            for item in items:
                cluster = find_news_cluster(item)
                if cluster is None:
                    continue

                # most clusters have short numeric ids, but some have very long
                # alphanumeric names. This can mess up with paths, so we
                # convert them to shorter names
                if len(cluster) > 20:
                    cluster = utils.create_cluster_name(output_dir)

                title = item.find('title').text
            
                logger.info('Accessing cluster {} - {}'.format(cluster, title))
                
                # the title itself has a link, but it is also among the ones in
                # the item description
                articles = extract_articles(item.find('description').text,
                                            filtered_domains_re_lang,
                                            filtered_domains_lang)
                for article in articles:
                    logger.info('Accessing {}'.format(article.url))
                    
                    if is_repeated(cluster, article.url):
                        logger.info('Already accessed, skipping')
                        continue
                    
                    count += 1
                    try:
                        article.download()
                        article.parse()
                    except newspaper.article.ArticleException:
                        logger.error('Error downloading article')
                        continue

                    save_article(output_dir, cluster, article.text, 
                                 article.apparent_title)
        
            logger.info('{} articles saved'.format(count))
            save_accessed_urls()
