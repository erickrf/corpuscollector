# -*- coding: utf-8 -*-


u'''
Funções compartilhadas pelos módulos deste projeto.
'''

from urllib.request import urlopen
import requests
import re
import logging
import os

regexp_espaco = re.compile('\s')
regexp_invalido = re.compile(r'[\\/:\*\?"<>\|,\+]')

cluster_prefix = 'cluster-'

user_agent = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36'\
    '(KHTML, like Gecko) Chrome/42.0.2311.135 Safari/537.36'


def set_logger(logger_name):
    '''
    Set the logger object used by the script.
    '''
    logger = logging.getLogger(logger_name)
    
    handler = logging.StreamHandler()
    formatter = logging.Formatter('%(message)s')
    handler.setLevel(logging.INFO)    
    handler.setFormatter(formatter)
    
    logger.setLevel(logging.INFO) 
    logger.addHandler(handler)


def normaliza_nome_arquivo(nome):
    u'''
    Normaliza um nome de arquivo, removendo caracteres inválidos 
    ou problemáticos. Tambem remove múltiplos espaços.
    '''
    # remove quebras de linha, tabulação, etc
    nome = regexp_espaco.sub(' ', nome)
    
    # remove caracteres invalidos
    nome = regexp_invalido.sub('-', nome)
    return nome


def create_cluster_name(output_dir):
    '''
    Create a name not currently in use for any cluster in the cluster directory.
    '''
    custom_names = [c for c in os.listdir(output_dir)
                    if c.startswith(cluster_prefix)]

    num_existing_clusters = len(custom_names)
    new_cluster = cluster_prefix + str(num_existing_clusters + 1)

    return new_cluster


def acessa_conteudo(url):
    '''
    Acessa a url desejada e retorna seu conteúdo.
    '''
    headers = {'user-agent': user_agent}
    r = requests.get(url, headers=headers)

    return r.text


def extrai_texto(soup):
    '''
    Extrai todo o conteúdo de texto de um objeto BeautifulSoup, 
    incluindo tags filhas. Tags <br/> são convertidas em quebras
    de linha.
    '''
    for br in soup.find_all('br'):
        br.replace_with('\n')
    
    texto = soup.text.strip()
    return texto


